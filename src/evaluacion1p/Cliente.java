/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evaluacion1p;

import java.util.Date;
import java.util.List;

/**
 *
 * @author USUARIO
 */
public class Cliente {
    
    private String Cedula;
    private String Nombres;
    private String Apellidos;
    private String Email;
    private String Contrasena;
    
    List<Reserva> ListaReservas;
    
    public void AgregarReserva(Reserva NuevaReserva)
    {
        ListaReservas.add(NuevaReserva);
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getContrasena() {
        return Contrasena;
    }

    public void setContrasena(String Contrasena) {
        this.Contrasena = Contrasena;
    }
    
    
}

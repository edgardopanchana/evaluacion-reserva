/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package evaluacion1p;

import java.util.ArrayList;

/**
 *
 * @author USUARIO
 */
public class Evaluacion1P {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Smartphone GalaxyS10 = new Smartphone();
        GalaxyS10.setMarca("Samsung");
        GalaxyS10.setModelo("S10");
        GalaxyS10.setTipo("Smartphone");
        GalaxyS10.setPrecio(1000);
        
         Smartphone GalaxyS9 = new Smartphone();
        GalaxyS9.setMarca("Samsung");
        GalaxyS9.setModelo("S9");
        GalaxyS9.setTipo("Smartphone");
        GalaxyS9.setPrecio(600);
        
        
         SmartTV LG = new SmartTV();
        LG.setMarca("LG");
        LG.setModelo("R300");
        LG.setTipo("SmarTV");
        LG.setPrecio(400);
        
        
        Reserva PrimeraReserva = new Reserva(10, GalaxyS10);
        Reserva SegundaReserva = new Reserva(2, GalaxyS9);
        Reserva TerceraReserva = new Reserva(5, LG);
        
        
        
        Cliente JeanPool = new Cliente();
        JeanPool.setApellidos("Conforme");
        JeanPool.setNombres("Jean Pool");
        JeanPool.setCedula("1112223334");
        
        JeanPool.ListaReservas = new ArrayList<>();
        JeanPool.ListaReservas.add(PrimeraReserva);
        JeanPool.ListaReservas.add(SegundaReserva);
        JeanPool.ListaReservas.add(TerceraReserva);
        
        Pedido miPedido = new Pedido(JeanPool);
        ImprimirPedido imprimir = new ImprimirPedido();
       imprimir.Imprimir(JeanPool);
        
        
    }
    
}
